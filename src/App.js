import React from "react";
import axios from "axios";

import MasterLayout from "./layouts/admin/MasterLayout";
import Login from './components/frontend/auth/Login';
import Register from './components/frontend/auth/Register';
import Home from "./components/frontend/Home";

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { publicRoutes } from './Routes/route'
import { RequireAuth } from "./Routes/RequireAuth";
import { AdminPrivateRoute } from "./Routes/AdminPrivateRoute";

axios.defaults.baseURL = 'http://localhost:8000'
axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.headers.post['Accept'] = 'application/json'
axios.defaults.withCredentials = true;
axios.interceptors.request.use((config) => {
  const token = localStorage.getItem('auth_token')
  config.headers.Authorization = token ? `Bearer ${token}` : ''
  return config
})

function App() {

  return (
    <Router>
      <Routes>
        <Route path="/"
          element={<Home />}
        />
        <Route path="/login"
          element={
            <RequireAuth>
              <Login />
            </RequireAuth>
          }
        />

        <Route path="/register"
          element={
            <RequireAuth>
              <Register />
            </RequireAuth>
          }
        />


        
        <Route path='/admin' element={<MasterLayout />}>
          {
            publicRoutes.map((route, index) => {
              return <Route key={index} path={route.path}
                element={<AdminPrivateRoute><route.component /></AdminPrivateRoute>}
              />
            })
          }
        </Route>
       
      </Routes>
    </Router>
  );
}

export default App;
