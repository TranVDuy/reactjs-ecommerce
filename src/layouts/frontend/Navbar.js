import axios from "axios";
import React, { useState } from "react";
import { Link, NavLink, useNavigate, Navigate, useLocation } from "react-router-dom";
import swal from "sweetalert";

function Navbar() {

    const navigate = useNavigate()
    let location = useLocation();

    const [isLoading, setLoading] = useState(false)

    const logoutSubmit = (e) => {
        setLoading(true)
        axios.post('/api/logout').then((res) => {
            if (res.data.status === 200) {
                localStorage.removeItem('auth_token')
                localStorage.removeItem('auth_name')
                    (<navigate to="/" />)

                swal('Success', res.data.message, 'success')
            }
            setLoading(false)
        })
            .catch((err) => { console.log(err); setLoading(false) })


    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary shadow sticky-top">
            <div className="container">
                <Link className="navbar-brand" to="#">Navbar</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink className="nav-link " aria-current="page" to="/">Home</NavLink>
                        </li>

                        {
                            localStorage.getItem('auth_token') ?
                                <>
                                    <li className="nav-item">
                                        <NavLink className="nav-link">{localStorage.getItem('auth_name')}</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        {
                                            isLoading ?
                                                <button
                                                    className="nav-link btn btn-danger btn-sm text-white disabled"
                                                >
                                                    Logout
                                                </button>
                                                :
                                                <button
                                                    className="nav-link btn btn-danger btn-sm text-white"
                                                    onClick={logoutSubmit}
                                                >
                                                    Logout
                                                </button>
                                        }

                                    </li>
                                </>

                                :

                                <>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/login">Login</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/register">Register</NavLink>
                                    </li>

                                </>

                        }


                    </ul>

                </div>
            </div>
        </nav>

    );
}

export default Navbar;