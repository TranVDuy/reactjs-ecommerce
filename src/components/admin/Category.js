import React from "react";

function Category() {
    return (
        <div className="container-fluid px-4">
            <h2 className="mt-4">Categories</h2>
            <div>
                <nav>
                    <div className="nav nav-tabs" id="nav-tab" role="tablist">
                        <button className="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Home</button>
                        <button className="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">SEO Tags</button>
                    </div>
                </nav>
                <div className="tab-content" id="nav-tabContent">
                    <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <form>
                            <div className="mb-3">
                                <label htmlFor="slugTextInput" className="form-label">Slug</label>
                                <input type="text" name="slug" id="slugTextInput" className="form-control" />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="nameTextInput" className="form-label">Name</label>
                                <input type="text" name="name" id="nameTextInput" className="form-control" />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="descriptionTextInput" className="form-label">Description</label>
                                <textarea className="form-control" name="description" id="descriptionTextInput" style={{ height: '100px' }}>
                                </textarea>
                            </div>
                            <div>
                                <h5>Status</h5>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="status" id="flexRadioDefault1" />
                                    <label className="form-check-label" htmlFor="flexRadioDefault1">
                                        Show
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="status" id="flexRadioDefault2" defaultChecked />
                                    <label className="form-check-label" htmlFor="flexRadioDefault2">
                                        Hidden
                                    </label>
                                </div>
                            </div>


                            <button type="submit" className="btn btn-primary">Submit</button>
                        </form>

                    </div>
                    <div className="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div className="mb-3">
                            <label htmlFor="nameTextInput" className="form-label">Meta title</label>
                            <input type="text" name="meta_title" id="nameTextInput" className="form-control" />
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="metaKeyWordsTextInput" className="form-label">Meta Keywords</label>
                            <textarea className="form-control" name="meta_keyword" id="metaKeyWordsTextInput" style={{ height: '80px' }}>
                            </textarea>
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="metaDescriptTextInput" className="form-label">Meta Description</label>
                            <textarea className="form-control" name="meta_descrip" id="metaDescriptTextInput" style={{ height: '80px' }}>
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    );
}

export default Category;