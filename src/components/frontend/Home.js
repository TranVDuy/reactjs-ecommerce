import React from "react";

import Navbar from "../../layouts/frontend/Navbar";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Home() {
    // Change title
    document.title = 'Home'
    return (
        <div>
            <Navbar />
            <h1>Home page</h1>
        </div>
    );
}

export default Home;