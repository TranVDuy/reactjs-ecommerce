import axios from "axios";
import React, { useState } from "react";
import Navbar from "../../../layouts/frontend/Navbar";
import swal from "sweetalert";
import {useNavigate} from 'react-router-dom'

function Login() {
    // Change title
    document.title = 'Login'
    const navigate = useNavigate()

    const [loginInput, setLoginInput] = useState({
        email: '',
        password: ''
    })

    const [validation, setValidation] = useState({
        email: '',
        password: '',
        fail: ''
    })

    const [isloading, setLoading] = useState(false)

    const handleInput = (e) => {
        e.persist()
        setLoginInput({ ...loginInput, [e.target.name]: e.target.value })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        const dataLogin = {
            email: loginInput.email,
            password: loginInput.password
        }
        setLoading(true)
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post('/api/login', dataLogin)
                .then((res) => {
                    if (res.data.status === 200) {
                        setValidation({
                            ...validation,
                            email: '',
                            password: '',
                            fail: ''
                        })
                        localStorage.setItem('auth_token', res.data.token)
                        localStorage.setItem('auth_name', res.data.username)
                        if(res.data.role === 1){
                            navigate('/admin/dashboard')
                        }
                        else{
                            navigate('/')
                        }
                        
                        
                        swal("Success!", `${res.data.message}`, "success");
                        
                    }
                    else {
                        if (res.data.status === 401) {
                            setValidation({
                                ...validation,
                                email: '',
                                password: '',
                                fail: res.data.message
                            })
                        }
                        else {
                            setValidation({
                                ...validation,
                                email: res.data.validatior_errors.email,
                                password: res.data.validatior_errors.password
                            })
                        }
                    }
                    setLoading(false)
                })
                .catch((err) => { console.log(err); setLoading(false) })
        })
        .catch((err) => { console.log(err); setLoading(false) })
    }

    return (
        <div>
            <Navbar />
            <div className="container py-5">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="card-header">
                            <h4>Login</h4>
                        </div>
                        <div className="card-body">
                            <form>
                                <div className="mb-3">
                                    <label>Email</label>
                                    <input autoFocus
                                        onChange={handleInput}
                                        value={loginInput.email}
                                        type="email"
                                        name="email"
                                        className="form-control" />
                                    <div className="form-text text-danger">{validation.email}</div>
                                </div>
                                <div className="mb-3">
                                    <label>Password</label>
                                    <input type="password"
                                        onChange={handleInput}
                                        value={loginInput.password}
                                        name="password"
                                        className="form-control" />
                                    <div className="form-text text-danger">{validation.password}</div>
                                </div>
                                <div className="mb-3">
                                    <div className="form-text text-danger">{validation.fail}</div>
                                </div>
                                {
                                    (isloading) ?
                                        <button
                                            type="submit"
                                            className="btn btn-primary disabled">
                                            Watting...
                                        </button>
                                        :
                                        <button
                                            onClick={handleSubmit}
                                            type="submit"
                                            className="btn btn-primary">
                                            Submit
                                        </button>
                                }

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;