import axios from "axios";
import React, { useState } from "react";
import {useNavigate} from 'react-router-dom'
import swal from "sweetalert";

import Navbar from "../../../layouts/frontend/Navbar";

function Register() {

    // Change title
    document.title = 'Register'

    const navigate = useNavigate()
    
    const [registerInput, setRegisterInput] = useState(
        {
            name: '',
            email: '',
            password: '',
        }
    );

    const [validator, setValidator] = useState(
        {
            errorList: []
        }
    )

    const [loading, setLoading] = useState(false)

    const handleInput = (e) => {
        setRegisterInput({ ...registerInput, [e.target.name]: e.target.value })
    }

    const handleRegisterSubmit = (e) => {
        e.preventDefault()
     

        const data = {
            name: registerInput.name,
            email: registerInput.email,
            password: registerInput.password,
        }
        setLoading(true)
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post('/api/register', data).then(res => {
                if (res.data.status === 200) {
                    localStorage.setItem('auth_token', res.data.token)
                    localStorage.setItem('auth_name', res.data.username)

                    setRegisterInput({
                        name: '',
                        email: '',
                        password: '',
                    })
                    swal("Success!", `${res.data.message}`, "success");
                    navigate('/')
                    
                }
                else {
                    setValidator({ ...validator, errorList: res.data.validation_errors })
                }
                setLoading(false)
            })
            .catch((err) => {console.log(err); setLoading(false)})
        }).catch((err) => {console.log(err); setLoading(false)});
      

    }


    return (
        <div>
            <Navbar />
            <div className="container py-5">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="card-header">
                            <h4>Register</h4>
                        </div>
                        <div className="card-body">
                            <form onSubmit={handleRegisterSubmit}>
                                <div className="mb-3">
                                    <label>Full name<span className="text-danger">(*)</span></label>
                                    <input autoFocus onChange={handleInput} value={registerInput.name} type="text" name="name" className="form-control" />
                                    <div className="form-text text-danger">{validator.errorList.name}</div>
                                </div>
                                <div className="mb-3">
                                    <label>Email<span className="text-danger">(*)</span></label>
                                    <input onChange={handleInput} value={registerInput.email} type="email" name="email" className="form-control" />
                                    <div className="form-text text-danger">{validator.errorList.email}</div>
                                </div>
                                <div className="mb-3">
                                    <label>Password<span className="text-danger">(*)</span></label>
                                    <input onChange={handleInput} value={registerInput.password} type="password" name="password" className="form-control" />
                                    <div className="form-text text-danger">{validator.errorList.password}</div>

                                </div>
                                {
                                    loading === true ?
                                        <button type="submit" className="btn btn-primary disabled">Waiting...</button>
                                        :
                                        <button type="submit" className="btn btn-primary">Register</button>
                                }
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;