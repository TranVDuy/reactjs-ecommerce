import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";

export function AdminPrivateRoute({ children }) {
    const navigate = useNavigate()

    const [isLoading, setLoading] = useState(false)

    const [authenticated, setAuthenticated] = useState(false)

    useEffect(() => {
        setLoading(true)
        axios.get('/api/checkingAuthenticated').then((res) => {
            if (res.status === 200) {
                setAuthenticated(true)
            }
            setLoading(false)
        })
            .catch((err) => { console.log(err); setLoading(false) })

        return () => {
            setAuthenticated(false)
            setLoading(false)
        }

    }, [])

    axios.interceptors.response.use(undefined, function axiosRetryInterceptor(err) {
        if (err.response.status === 401) {
            // <Navigate to="/login" state={{ from: location }} replace />
            navigate('/login')
            swal('Unauthorized', err.response.data.message, 'warning');
        }
        return Promise.reject(err)
    })

    
    axios.interceptors.response.use(function (res) {
        return res;
    }, function (err) {
        if (err.response.status === 403) //Access Denied
        {
            // <Navigate to="/" state={{ from: location }} replace />
            navigate('/')
            swal('Forbidden', err.response.data.message, 'warning')
        }
        else {
            if (err.response.status === 404) //Page not found
            {
                swal('404 Error', 'Page not found', 'warning')
            }
            else{
                if (err.response.status === 401) {
                    // <Navigate to="/login" state={{ from: location }} replace />
                    navigate('/login')
                    swal('Unauthorized', err.response.data.message, 'warning');
                }
            }
        }

        return Promise.reject(err)
    })

    if (isLoading) {
        return <h1>Loading...</h1>
    }

    return (
        { authenticated } ?
            children
            :
            // <Navigate to='/login' state={{ from: location }} replace />
            navigate('/')
    );

}

