import Category from '../components/admin/Category';
import Dashboard from '../components/admin/Dashboard';
import Profile from '../components/admin/Profile';

const publicRoutes = [
    { path: '/admin/dashboard', component: Dashboard, name:'Dashboard'},
    { path: '/admin/categories', component: Category, name:'Category'},
    { path: '/admin/profile', component: Profile, name:'Profile'},
];

export {publicRoutes}